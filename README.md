# my-lang

Messing around with making a basic interpreted functional language.

I'm using this as a way to learn the language [Idris2](https://www.idris-lang.org).


## Useful Links

- [Lambda Calculus](https://en.wikipedia.org/wiki/Lambda_calculus)
- [De Bruijn Indexing](https://en.wikipedia.org/wiki/De_Bruijn_index)
- [Implementing Dependent Type Theory - Part 3](http://math.andrej.com/2012/11/29/how-to-implement-dependent-type-theory-iii/)
  *Includes a discussion on implementing De Bruijn Indexing.*
- [Typed Lambda Calculus](https://en.wikipedia.org/wiki/Typed_lambda_calculus)
- [Simply Typed Lambda Calculus](https://en.wikipedia.org/wiki/Simply_typed_lambda_calculus)
- [Idris2 Docs/Tutorial](https://idris2.readthedocs.io/en/latest/tutorial/index.html)

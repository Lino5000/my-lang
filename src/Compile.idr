module Compile

import Parsing
import Logic
import Result

import Data.Either
import Data.Fin
import Data.List
import Data.List1
import Data.String
import Text.Parser.Core

compile' : (numFree : Nat) -> (bindings : List String) -> (stmt : Stmt) -> (p : Program) -> (LangRes Expr, Nat)
compile' numFree bindings (SVar name) _ = case findIndex (== name) bindings of
                                       Just idx => (Right . Var $ finToNat idx, numFree)
                                       _ => (Right . Var $ numFree + 1, numFree + 1)
compile' numFree bindings (SFuncApp func arg) p = case compile' numFree bindings func p of
                                                       (Left err, free) => (Left err, free)
                                                       (Right f, newFree) => case compile' newFree bindings arg p of
                                                                                  (Left err, free) => (Left err, free)
                                                                                  (Right a, newFree') => (Right (FuncApp f a), newFree')
compile' numFree bindings (SBind name body) p = case compile' numFree (name :: bindings) body p of
                                                     (Left err, free) => (Left err, free)
                                                     (Right expr, newFree) => (Right (Bind expr), newFree)
compile' numFree bindings (SIdent name) p = case lookup name $ forget p of
                                                 Nothing => (Left . CompileErr $ "No identifier `" ++ name ++"` found in program.", numFree)
                                                 Just stmt => compile' numFree bindings stmt p

export
compileStmt : (stmt : Stmt) -> (p : Program) -> (LangRes Expr, Nat)
compileStmt = compile' 0 []

export
eval : (p : Program) -> LangRes Expr
eval p = case lookup "MAIN" $ forget p of
              Nothing => Left . CompileErr $ "The program does not contain a `MAIN` statement."
              Just stmt => case compileStmt stmt p of
                                (Left err, _) => Left err
                                (Right expr, _) => Right (reduceFull expr)

export
Context : Type
Context = List1 (Expr, String)

export
compileProg : (p : Program) -> LangRes Context
compileProg p = case partitionEithers $ forget exprs of
                     ([], rs) => case fromList rs of
                                      Nothing => Left $ CompileErr "Something went terribly wrong, and the program is somehow empty."
                                      Just ctxt => Right $ reduceCtxt ctxt
                     ((err :: errs), _) => Left err
  where
    revert : (LangRes Expr, String) -> LangRes (Expr, String)
    revert ((Left err), _) = Left err
    revert ((Right res), name) = Right (res, name)
    exprs : List1 (LangRes (Expr, String))
    exprs = map (\(name, stmt) => revert (fst $ compileStmt stmt p, name)) p
    reduceCtxt : Context -> Context
    reduceCtxt = map (\(expr, name) => (reduceFull expr, name))

bindName : (idx : Nat) -> String
bindName = singleton . chr . (+ 97) . fromInteger . natToInteger

Context' : Type
Context' = List (Expr, String)

mutual
  applyContext'' : (expr : Expr) -> (ctxt : Context') -> (binds : List String) -> Stmt
  applyContext'' (Var idx) ctxt binds = case inBounds idx binds of
                                             Yes prf => SVar $ index idx binds
                                             No _ => SVar ("f" ++ bindName idx)
  applyContext'' (FuncApp func arg) ctxt binds = SFuncApp (applyContext' func ctxt binds) (applyContext' arg ctxt binds)
  applyContext'' (Bind body) ctxt binds = let name = bindName $ length binds in
                                              SBind name . applyContext' body ctxt $ name :: binds

  applyContext' : (expr : Expr) -> (ctxt : Context') -> (binds : List String) -> Stmt
  applyContext' expr ctxt binds = case lookup expr ctxt of
                              Just name => SIdent name
                              Nothing => applyContext'' expr ctxt binds

export
applyContext : (ctxt : Context) -> (expr : Expr) -> Stmt
applyContext ctxt expr = applyContext' expr ctxt' [] where
  ctxt' : Context'
  ctxt' = filter (\(_, name) => name /= "MAIN") $ forget ctxt

export
strToProgram : (source : String) -> LangRes Program
strToProgram source = case lex source of
                           (tokns, (_, (_, ""))) => case parse tokns of
                                                         -- TODO: Improve error messages
                                                         Left errs => Left . ParseErr $ "Token Errors" ++ errsString errs
                                                         Right (prog, []) => Right prog
                                                         Right (_, tkns) => Left . ParseErr $ "Unused tokens: " ++ show tkns
                           -- TODO: Better error message, left = (ln, (col, rem))
                           (_, left) => Left . LexerErr $ show left

export
compileAndEvaluate : (source : String) -> LangRes Expr
compileAndEvaluate source = do p <- strToProgram source
                               eval p

ctxt_test : IO ()
ctxt_test = case strToProgram "SUCC := \\n. \\f. \\x. (f ((n f) x)); 0 := \\f. \\x. x; 1 := (SUCC 0); 2 := (SUCC 1); 3 := (SUCC 2); MAIN := (SUCC 3);" of
                 Left err => putStrLn $ "[ERROR] strToProgram: " ++ show err
                 Right prog => case compileProg prog of
                                    Left err => putStrLn $ "[ERROR] compileProg: " ++ show err
                                    Right ctxt => case lookup "MAIN" . map (\(a,b) => (b,a)) $ forget ctxt of
                                                       Nothing => putStrLn "[ERROR] applyContext"
                                                       Just expr => printLn $ applyContext ctxt expr

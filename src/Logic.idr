module Logic

import Data.Stream

import Parsing.AST

export
Sub : Type
Sub = Stream Expr

export
shift : (k : Nat) -> Sub
shift = map (Var) . rangeFrom

export
listSub : (hs : List Expr) -> Sub
listSub hs = listSub' 0 hs where
  listSub' : Nat -> List Expr -> Sub
  listSub' k [] = shift k
  listSub' k (x :: xs) = x :: listSub' (k + 1) xs

decFree : Expr -> Expr -- Decrements all the free variables in the expression by 1
decFree = decFree' 0 where
  decFree' : (depth : Nat) -> Expr -> Expr
  decFree' depth (Var (S idx)) = if idx >= depth
                                    then Var idx      -- Free
                                    else Var (S idx)  -- Bound
  decFree' _ (Var 0) = Var 0  -- Can't decrement 0, might break if applied to something other than bind
  decFree' depth (FuncApp func arg) = FuncApp (decFree' depth func) (decFree' depth arg)
  decFree' depth (Bind body) = Bind (decFree' (depth + 1) body)

mutual
  export
  composeSub : (a : Sub) -> (b : Sub) -> Sub
  composeSub a b = map (\expr => substitute expr b) a

  substitute' : (expr : Expr) -> (subs : Sub) -> Expr
  substitute' (Var idx) subs = index idx subs
  substitute' (FuncApp func arg) subs = FuncApp (substitute' func subs) (substitute' arg subs)
  substitute' (Bind body) subs = Bind (substitute' body $ (Var 0) :: subs') where
    subs' : Sub
    subs' = composeSub subs $ shift 1

  export
  substitute : (expr : Expr) -> (subs : Sub) -> Expr
  substitute (Bind body) subs = decFree $ substitute' (Bind body) subs
  substitute expr subs = substitute' expr subs

usesBind : (idx : Nat) -> (f : Expr) -> Bool
usesBind idx (Var k) = k == idx
usesBind idx (FuncApp func arg) = (usesBind idx func) || (usesBind idx arg)
usesBind idx (Bind body) = usesBind (idx + 1) body

export
reduce : Expr -> Expr
reduce (Var idx) = Var idx
reduce (FuncApp (Bind func) arg) = substitute func $ listSub [arg] -- beta-reduction
reduce (FuncApp func arg) = FuncApp (reduce func) (reduce arg)
reduce (Bind body) = Bind (reduce body)
{-reduce (Bind (FuncApp f (Var 0))) = if usesBind 0 f
                                         then Bind (FuncApp f (Var 0))
                                         else f -- eta-reduction
                                         -}

export
reduceIter : (n : Nat) -> Expr -> Expr
reduceIter 0 = id
reduceIter (S k) = reduceIter k . reduce

export
reduceFull : Expr -> Expr
reduceFull ex = let reduct = reduce ex in if ex == reduct
                                             then ex
                                             else reduceFull reduct

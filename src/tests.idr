import Parsing
import Logic

substitute_test : IO ()
substitute_test = let f = FuncApp (Var 1) (Var 0) in
                      do printLn . substitute (Var 0) $ listSub [Bind (Bind (f))] -- Should replace with \ \ (1 0)
                         printLn . substitute (Var 5) $ listSub [Bind (Bind (f))] -- Shouldn't change
                         printLn . substitute (FuncApp (Var 0) (Var 5)) $ listSub [Bind (Bind (f))] -- Should change function and not variable
                         printLn . substitute (Bind (Var 0)) $ listSub [Bind (Bind (f))] -- Shouldn't change
                         printLn . substitute (Bind (f)) $ listSub [Bind (Bind (f))] -- Should change function
                         printLn . substitute (Bind (FuncApp (Var 1) (FuncApp (FuncApp (Bind (Bind (Var 0))) (Var 1)) (Var 0)))) $ listSub [Var 1]
                           -- ((λ (λ (1 (((λ (λ 0)) 1) 0)))) 1)

trace_reduce : Expr -> IO ()
trace_reduce expr = do printLn expr
                       let reduct = reduce expr
                       if reduct == expr
                          then putStrLn "Done!"
                          else trace_reduce reduct

reduce_test : IO ()
reduce_test = do let plus = Bind (Bind (Bind (Bind (FuncApp (FuncApp (Var 3) (Var 1)) (FuncApp (FuncApp (Var 2) (Var 1)) (Var 0))))))
                 let succ = Bind (Bind (Bind (FuncApp (Var 1) (FuncApp (FuncApp (Var 2) (Var 1)) (Var 0)))))
                 let zero = Bind (Bind (Var 0))
                 let one = FuncApp (succ) (zero)
                 let two = FuncApp (succ) (one)
                 putStrLn "plus:"
                 trace_reduce plus
                 putStrLn "succ:"
                 trace_reduce succ
                 putStrLn "zero:"
                 trace_reduce zero
                 putStrLn "one:"
                 trace_reduce one
                 putStrLn "two:"
                 trace_reduce two
                 putStrLn "0 + 0:"
                 trace_reduce $ FuncApp (FuncApp plus zero) zero
                 putStrLn "1 + 0:"
                 trace_reduce $ FuncApp (FuncApp plus one) zero
                 putStrLn "0 + 1:"
                 trace_reduce $ FuncApp (FuncApp plus zero) one
                 putStrLn "1 + 1:"
                 trace_reduce $ FuncApp (FuncApp plus one) one

module Main

import Parsing
import Compile
import Result

import System
import System.File.ReadWrite

runFromFile : (filepath : String) -> IO (LangRes Expr)
runFromFile filepath = do Right source <- readFile filepath
                          | Left err => pure . Left . FileErr $ show err
                          pure $ compileAndEvaluate source

sample : IO ()
sample = do res <- runFromFile "sample/basic_syntax.mlang"
            putStrLn $ showRes res

main : IO ()
main = do [_, source_file] <- getArgs
          | _ => putStrLn "Invalid command-line options; please provide the path to a single input file."
          res <- runFromFile source_file
          putStrLn $ showRes res

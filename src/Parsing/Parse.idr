module Parsing.Parse

import Parsing.AST
import Parsing.Lexer
import Result

import Text.Parser
import Data.List1

identName : Grammar _ Token True String
identName = terminal "Invalid identifier name; should match /[A-Z0-9]+/" f where
  f : Token -> Maybe String
  f x = case x of
             (Ident name) => Just name
             _ => Nothing

ident : Grammar _ Token True Stmt
ident = do name <- identName
           pure $ SIdent name

assignOp : Grammar _ Token True ()
assignOp = terminal "Expected `:=` assignment" f where
  f : Token -> Maybe ()
  f x = case x of
             Assign => Just ()
             _ => Nothing

slash : Grammar _ Token True ()
slash = terminal "Expected `\\`" f where
  f : Token -> Maybe ()
  f x = case x of
             Slash => Just ()
             _ => Nothing

varName : Grammar _ Token True String
varName = terminal "Invalid variable name; should match /[a-z]+/" f where
  f : Token -> Maybe String 
  f x = case x of
             (Var name) => Just name
             _ => Nothing

var : Grammar _ Token True Stmt
var = do name <- varName
         pure $ SVar name

bindDot : Grammar _ Token True ()
bindDot = terminal "Expected `.`" f where
  f : Token -> Maybe ()
  f x = case x of
             Dot => Just ()
             _ => Nothing

lParen : Grammar _ Token True ()
lParen = terminal "Expected `(`"  f where
  f : Token -> Maybe ()
  f x = case x of
             LParen => Just ()
             _ => Nothing

rParen : Grammar _ Token True ()
rParen = terminal "Expected `)` (might not be properly closed)"  f where
  f : Token -> Maybe ()
  f x = case x of
             RParen => Just ()
             _ => Nothing

semiColon : Grammar _ Token True ()
semiColon = terminal "Expected `;`" f where
  f : Token -> Maybe ()
  f x = case x of
             SemiColon => Just ()
             _ => Nothing

mutual
  funcApp : Grammar _ Token True Stmt
  funcApp = do lParen
               func <- statement
               arg <- statement
               rParen
               pure $ SFuncApp func arg

  binding : Grammar _ Token True Stmt
  binding = do slash
               name <- varName
               bindDot
               body <- statement
               pure $ SBind name body

  statement : Grammar _ Token True Stmt
  statement =  var
           <|> funcApp
           <|> binding
           <|> ident

assignment : Grammar _ Token True (String, Stmt)
assignment = do name <- identName
                assignOp
                def <- statement
                semiColon
                pure $ (name, def)

program : Grammar _ Token True Program
program = some assignment

export
parse : List (WithBounds Token) -> Either (List1 (ParsingError Token)) (Program, List (WithBounds Token))
parse = Text.Parser.Core.parse program

export
errString : ParsingError Token -> String
errString (Error str bounds) = case bounds of
                                    Nothing => str
                                    Just bnd => show bnd ++ ": " ++ str

export
errsString : List1 (ParsingError Token) -> String
errsString errs = foldl (\acc,err => acc ++ "\n  " ++ errString err) "" errs

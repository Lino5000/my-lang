module Parsing.Lexer

import Data.List
import Text.Lexer
import Text.Bounded

public export
data Token : Type where
  Ident : (name : String) -> Token  -- All caps + numbers
  Assign : Token  -- `:=`
  Slash : Token  -- `\`
  Var : (name : String) -> Token  -- Lower case
  Dot : Token  -- `.`
  LParen : Token  -- `(`
  RParen : Token  -- `)`
  SemiColon : Token  -- ';' at the end of a line
  Space : Token  -- ' '

export
covering
Show Token where
  show (Ident name) = "Ident `" ++ name ++ "`"
  show Assign = "Assign"
  show Slash = "Slash"
  show (Var name) = "Var `" ++ name ++ "`"
  show Dot = "Dot"
  show LParen = "LParen"
  show RParen = "RParen"
  show SemiColon = "SemiColon"
  show Space = "Space"

relevant : WithBounds Token -> Bool
relevant (MkBounded Space _ _) = False
relevant _ = True

ident : Lexer
ident = some $ upper <|> digit

assign : Lexer
assign = exact ":=" 

slash : Lexer
slash = is '\\'

var : Lexer
var = lowers

dot : Lexer
dot = is '.'

lParen : Lexer
lParen = is '('

rParen : Lexer
rParen = is ')'

semiColon : Lexer
semiColon = is ';'

tokenMap : TokenMap Token
tokenMap = [ (ident, Ident)
           , (assign, const Assign)
           , (slash, const Slash)
           , (var, Var)
           , (dot, const Dot)
           , (lParen, const LParen)
           , (rParen, const RParen)
           , (semiColon, const SemiColon)
           , (space, const Space)
           ]

clean : (List (WithBounds Token), (Int, (Int, String)))
        -> (List (WithBounds Token), (Int, (Int, String)))
clean (tokens, rem) = (filter relevant tokens, rem)

export
lex : (inp : String) -> (List (WithBounds Token), (Int, (Int, String)))
lex = clean . lex tokenMap

lex_example : IO ()
lex_example = printLn . map (\(MkBounded tok _ _) => tok) . fst $ lex "SUCC := \\n. \\f. \\x. (f ((n f) x)); 0 := \\f. \\x. (x); MAIN := SUCC 0"

module Parsing.AST

import Data.List1

public export
data Expr : Type where
  Var     : (idx : Nat) -> Expr -- Variables use De Bruijn indexing
  FuncApp : (func : Expr) -> (arg : Expr) -> Expr
  Bind    : (body : Expr) -> Expr

export
covering
Show Expr where
  show (Var idx) = show idx
  show (FuncApp func arg) = "(" ++ show func ++ " " ++ show arg ++ ")"
  show (Bind body) = (pack [chr 0x03BB, ' ']) ++ show body

export
covering
Eq Expr where
  (==) (Var a) (Var b) = a == b
  (==) (FuncApp f1 a) (FuncApp f2 b) = f1 == f2 && a == b
  (==) (Bind a) (Bind b) = a == b
  (==) _ _ = False

public export
data Stmt : Type where
  SVar     : (name : String) -> Stmt -- Variables use De Bruijn indexing
  SFuncApp : (func : Stmt) -> (arg : Stmt) -> Stmt
  SBind    : (boundName : String) -> (body : Stmt) -> Stmt
  SIdent   : (name : String) -> Stmt

export
covering
Show Stmt where
  show (SVar name) = name
  show (SFuncApp func arg) = "(" ++ show func ++ " " ++ show arg ++ ")"
  show (SBind name body) = "(" ++ (pack [chr 0x03BB]) ++ name ++ " " ++ show body ++ ")"
  show (SIdent name) = name

export covering
Eq Stmt where
  (==) (SVar a) (SVar b) = a == b
  (==) (SFuncApp f1 a) (SFuncApp f2 b) = f1 == f2 && a == b
  (==) (SBind n a) (SBind m b) = a == b && n == m
  (==) (SIdent a) (SIdent b) = a == b
  (==) _ _ = False

public export
Program : Type
Program = List1 (String, Stmt)

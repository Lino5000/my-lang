module Result

import Parsing.AST

public export
data LangErr : Type where
  CompileErr : (errMsg : String) -> LangErr
  LexerErr   : (errMsg : String) -> LangErr
  ParseErr   : (errMsg : String) -> LangErr
  FileErr    : (errMsg : String) -> LangErr

export
covering
Show LangErr where
  show (CompileErr msg) = "Compile Error:\n" ++ msg
  show (LexerErr msg) = "Lexer Error:\n" ++ msg
  show (ParseErr msg) = "Parser Error:\n" ++ msg
  show (FileErr msg) = "File IO Error:\n" ++ msg

public export
LangRes : (a : Type) -> Type
LangRes a = Either LangErr a

public export
showRes : Show a => LangRes a -> String
showRes (Left err) = show err
showRes (Right res) = show res
